package cn.codetector.util.GLutil.BasicShape;

import cn.codetector.util.GLutil.TextureRenderer.TextureRenderer;
import cn.codetector.util.Graph.Color.Color3;
import cn.codetector.util.Graph.Vector.Vec2;

import java.awt.image.BufferedImage;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Codetector on 15/4/26.
 */
public class ShapeBox {
    public Vec2 Size;
    public Vec2 Position;
    public Color3 color;
    private String Texture;
    private int Texture_ID;
    public ShapeBox(Vec2 pos, Vec2 Size, int HexColor, String Texture){
        color = new Color3(HexColor);
        Position = pos;
        this.Size = Size;
        this.Texture = Texture;
        this.Texture_ID = TextureRenderer.BindImage(Texture);
    }
    public ShapeBox(Vec2 pos, Vec2 Size, int HexColor, int TextureID){
        color = new Color3(HexColor);
        Position = pos;
        this.Size = Size;
        this.Texture = "1";
        this.Texture_ID = TextureID;
    }

    public ShapeBox(Vec2 pos, Vec2 Size, int HexColor, BufferedImage Texture){
        color = new Color3(HexColor);
        Position = pos;
        this.Size = Size;
        this.Texture = "1";
        this.Texture_ID = TextureRenderer.BindImage(Texture);
    }

    public void MoveLocationFromVector(Vec2 v){
        this.Position = this.Position.Add(v);
    }

    public void draw(){
        if(this.Texture.equalsIgnoreCase("") || this.Texture == null){
            glColor3f(color.Red, color.Green, color.Blue);
        }else{
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, this.Texture_ID);
        }
        glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2f(this.Position.X, this.Position.Y);
        glTexCoord2f(1, 0);
        glVertex2f(this.Position.X+this.Size.GetWidthf(), this.Position.Y);
        glTexCoord2f(1,1);
        glVertex2f(this.Position.X+this.Size.GetWidthf(), this.Position.Y + this.Size.GetHeightf());
        glTexCoord2f(0,1);
        glVertex2f(this.Position.X, this.Position.Y + this.Size.GetHeightf());
        glEnd();
    }

    public void ReleaseTexture(){
        glDeleteTextures(this.Texture_ID);
    }
}
